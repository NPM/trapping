plugins {
    kotlin("jvm") version "1.9.20"
    application
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

group = "ru.inr.mass"
version = "1.1.1"

description = "Numass trapping simulation"

application {
    mainClass.set("ru.inr.mass.trapping.MainKt")
}

repositories {
    maven("https://repo.kotlin.link")
    mavenCentral()
}

dependencies {
    implementation("org.apache.commons:commons-math3:3.6.1")
    implementation("org.apache.commons:commons-rng-simple:1.1")
    implementation("space.kscience:plotlykt-core:0.5.0")
    implementation("de.m3y.kformat:kformat:0.7")
    testImplementation(kotlin("test-junit"))
}

kotlin{
    jvmToolchain(11)
}

